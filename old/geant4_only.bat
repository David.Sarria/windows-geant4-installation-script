REM Compile and install GEANT4
echo Compiling and Installing GEANT4 10_04_p03

set build_type=Release

set G4_bat_file_dir=%~dp0

cd %G4_bat_file_dir%\build

if exist CMakeCache.txt del CMakeCache.txt

setx PATH ";"
setx PATH "%G4_bat_file_dir%Qt4\install\bin;%G4_bat_file_dir%Qt4\install\lib;%PATH%"

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe ^
-DCMAKE_INSTALL_PREFIX=%G4_bat_file_dir%\install ^
-DCMAKE_PREFIX_PATH=%G4_bat_file_dir%\xerces-c\install\cmake ^
-DGEANT4_INSTALL_DATA=ON ^
-DGEANT4_USE_QT=ON ^
-DGEANT4_FORCE_QT4=ON ^
-DGEANT4_USE_OPENGL_WIN32=ON ^
-DGEANT4_USE_GDML=ON ^
-DXERCESC_INCLUDE_DIR=%G4_bat_file_dir%\xerces-c\install\include ^
-DXERCESC_LIBRARY=%G4_bat_file_dir%\xerces-c\install\lib\xerces-c_3.lib ^
-DGEANT4_BUILD_MSVC_MP=ON ^
-DCMAKE_CONFIGURATION_TYPES=%build_type% ^
%G4_bat_file_dir%\geant4_10_04_p03

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type%
%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type% --target install

cd %G4_bat_file_dir%

PAUSE