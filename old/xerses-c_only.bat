REM loading visual studio 2015 (v14) environment (to get nmake)
call "%vs140comntools%vsvars32.bat"

set G4_bat_file_dir=%~dp0

set build_type=RelWithDebInfo

REM Compile and install XERSEC-C
echo Compiling and Installing XERSEC-C
cd %G4_bat_file_dir%

if not exist %G4_bat_file_dir%\xerces-c\install mkdir %G4_bat_file_dir%\xerces-c\install
if not exist %G4_bat_file_dir%\xerces-c\build mkdir %G4_bat_file_dir%\xerces-c\build

cd %G4_bat_file_dir%\xerces-c\build

REM if exist CMakeCache.txt del CMakeCache.txt

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe -DCMAKE_INSTALL_PREFIX=%G4_bat_file_dir%\xerces-c\install ^
-DCMAKE_CONFIGURATION_TYPES=%build_type% ^
-DBUILD_SHARED_LIBS=ON ^
-DBUILD_STATIC_LIBS=OFF ^
%G4_bat_file_dir%\xerces-c\source

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type% --parallel 4
%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type% --target install
