set G4_bat_file_dir=%~dp0
echo %G4_bat_file_dir%

set G4dataset_RootDir=%G4_bat_file_dir%\install\share\Geant4-10.4.3\data\
setx G4ABLADATA "%G4dataset_RootDir%G4ABLA3.1"
setx G4ENSDFSTATEDATA "%G4dataset_RootDir%G4ENSDFSTATE2.2"
setx G4LEDATA "%G4dataset_RootDir%G4EMLOW7.3"
setx G4LEVELGAMMADATA "%G4dataset_RootDir%PhotonEvaporation5.2"
setx G4NEUTRONHPDATA "%G4dataset_RootDir%G4NDL4.5"
setx G4NEUTRONXSDATA "%G4dataset_RootDir%G4NEUTRONXS1.4"
setx G4PIIDATA "%G4dataset_RootDir%G4PII1.3"
setx G4RADIOACTIVEDATA "%G4dataset_RootDir%RadioactiveDecay5.2"
setx G4REALSURFACEDATA "%G4dataset_RootDir%RealSurface2.1.1"
setx G4SAIDXSDATA "%G4dataset_RootDir%G4SAIDDATA1.1"

setx PATH ";"
setx PATH "%G4_bat_file_dir%\xerces-c\instal\bin;%G4_bat_file_dir%\install\bin;%G4_bat_file_dir%\Qt4\install\bin;%PATH%"