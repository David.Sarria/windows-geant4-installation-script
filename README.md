# WINDOWS GEANT4 INSTALLATION SCRIPT

## Batch (.bat) script to compile and install GEANT4 on Windows with full functionallity (GDML, OpenGL, Qt). EXPERIMENTAL: probably still a few bugs. It may also mess up the Environment Variables... Use at your own risk.

* Requires Visual Studio 2015 (version 14.x). The installer is provided (vs_community.exe).

* To install, Right click on `compile_install_Full.bat` and "Run as administrator". Will take at least 1 hour to compile and install... If everything works fine, it should compile and open example B1.

* Use `launch_visual_studio.bat` with "Run as administrator" to launch Visual Studio IDE with the correct environment