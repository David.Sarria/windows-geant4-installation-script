@echo off
cls

REM setx is for permanent (future commands)
REM set is local (only this command execution)

REM loading visual studio 2015 (v14) environment (to get nmake)
call "%vs140comntools%vsvars32.bat"

REM debug mode is not able to compile because of limitation "too many sections on object file", "LINK : fatal error LNK1189: library limit of 65535 objects exceeded [C:\Users\testeur\Desktop\windows-geant4-installati on-script\build\source\processes\G4processes.vcxproj]""

REM set build_type=Release
REM set build_type=Debug
set build_type=RelWithDebInfo

set G4_bat_file_dir=%~dp0
setx G4_bat_file_dir %G4_bat_file_dir%
echo %G4_bat_file_dir%

cd %G4_bat_file_dir%

if not exist %G4_bat_file_dir%\install mkdir %G4_bat_file_dir%\install
if not exist %G4_bat_file_dir%\build mkdir %G4_bat_file_dir%\build

REM Set temporary environement varialbe PATH to find perl and CMake

IF EXIST %G4_bat_file_dir%\strawberry-perl\perl\bin SET PATH=%PATH%;%G4_bat_file_dir%\strawberry-perl\perl\bin
IF EXIST %G4_bat_file_dir%\cmake-3.14\bin SET PATH=%PATH%;%G4_bat_file_dir%\cmake-3.14\bin

where /q perl || ECHO Cound not find perl. Aborting. && PAUSE && EXIT /B

where /q cmake || ECHO Cound not find cmake. Aborting. && PAUSE && EXIT /B

REM Compile and install Qt4 4.8.7
echo Compiling and Installing Qt4 4.8.7
cd %G4_bat_file_dir%

set QTDIR=%G4_bat_file_dir%Qt4\qt-4.8.7_patched\
set QMAKESPEC=win32-msvc2015

setx QTDIR %G4_bat_file_dir%Qt4\qt-4.8.7_patched\
setx QMAKESPEC win32-msvc2015

cd %QTDIR%

set PATH=%PATH%;%QTDIR%bin;%QTDIR%lib

REM -debug-and-release

configure -platform win32-msvc2015 ^
-opensource -confirm-license -opengl desktop ^
-debug -mp ^
-nomake tests -nomake examples -nomake docs -nomake translations -nomake demos

REM -debug

nmake
nmake install

REM Compile and install XERSEC-C
echo Compiling and Installing XERSEC-C
cd %G4_bat_file_dir%

if not exist %G4_bat_file_dir%\xerces-c\install mkdir %G4_bat_file_dir%\xerces-c\install
if not exist %G4_bat_file_dir%\xerces-c\build mkdir %G4_bat_file_dir%\xerces-c\build

cd %G4_bat_file_dir%\xerces-c\build

REM if exist CMakeCache.txt del CMakeCache.txt

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe -DCMAKE_INSTALL_PREFIX=%G4_bat_file_dir%\xerces-c\install ^
-DCMAKE_CONFIGURATION_TYPES=%build_type% ^
%G4_bat_file_dir%\xerces-c\source

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type% --parallel 4
%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type% --target install

REM Compile and install GEANT4

echo Compiling and Installing GEANT4 10_04_p03

if not exist %G4_bat_file_dir%\build mkdir %G4_bat_file_dir%\build

if not exist %G4_bat_file_dir%\install mkdir %G4_bat_file_dir%\install

cd %G4_bat_file_dir%\build

REM if exist CMakeCache.txt del CMakeCache.txt

if %build_type%==Debug (set xerses_lib=xerces-c_3D.lib) else (set xerses_lib=xerces-c_3.lib)

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe ^
-DCMAKE_INSTALL_PREFIX=%G4_bat_file_dir%\install ^
-DCMAKE_PREFIX_PATH=%G4_bat_file_dir%\xerces-c\install\cmake ^
-DGEANT4_INSTALL_DATA=ON ^
-DGEANT4_USE_QT=ON ^
-DGEANT4_USE_OPENGL_WIN32=ON ^
-DGEANT4_USE_GDML=ON ^
-DXERCESC_INCLUDE_DIR=%G4_bat_file_dir%\xerces-c\install\include ^
-DXERCESC_LIBRARY=%G4_bat_file_dir%\xerces-c\install\lib\%xerses_lib% ^
-DQT_IMPORTS_DIR=%QTDIR%imports ^
-DQT_QMAKE_EXECUTABLE=%QTDIR%bin/qmake.exe ^
-DCMAKE_CONFIGURATION_TYPES=%build_type% ^
-DGEANT4_BUILD_MSVC_MP=ON ^
-DGEANT4_FORCE_QT4=ON ^
-DGEANT4_INSTALL_EXAMPLES=OFF ^
-DGEANT4_USE_G3TOG4=ON ^
-DGEANT4_BUILD_MULTITHREADED=OFF ^
%G4_bat_file_dir%\geant4_10_04_p03

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type%
%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type% --target install

cd %G4_bat_file_dir%

REM Setting up more environement variables
REM setx is for permanent (future commands)
REM set is local (only this command execution)

echo Setting environement variables

set PATH=%PATH%;%G4_bat_file_dir%install\bin;%QTDIR%bin;%G4_bat_file_dir%install\lib;%QTDIR%lib;%G4_bat_file_dir%install\include\Geant4

set G4dataset_RootDir=%G4_bat_file_dir%install\share\Geant4-10.4.3\data\

setx G4dataset_RootDir %G4_bat_file_dir%install\share\Geant4-10.4.3\data\

setx G4ENSDFSTATEDATA %%G4dataset_RootDir%%G4ENSDFSTATE2.2
setx G4LEDATA %%G4dataset_RootDir%%G4EMLOW7.3
setx G4LEVELGAMMADATA %%G4dataset_RootDir%%PhotonEvaporation5.2
setx G4SAIDXSDATA %%G4dataset_RootDir%%G4SAIDDATA1.1
setx G4NEUTRONHPDATA %%G4dataset_RootDir%%G4NDL4.5
setx G4NEUTRONXSDATA %%G4dataset_RootDir%%G4NEUTRONXS1.4
setx G4PIIDATA %%G4dataset_RootDir%%G4PII1.3
setx G4RADIOACTIVEDATA %%G4dataset_RootDir%%RadioactiveDecay5.2
setx G4REALSURFACEDATA %%G4dataset_RootDir%%RealSurface2.1
setx G4ABLADATA %%G4dataset_RootDir%%G4ABLA3.1

set G4ENSDFSTATEDATA=%%G4dataset_RootDir%%G4ENSDFSTATE2.2
set G4LEDATA=%%G4dataset_RootDir%%G4EMLOW7.3
set G4LEVELGAMMADATA=%%G4dataset_RootDir%%PhotonEvaporation5.2
set G4SAIDXSDATA=%%G4dataset_RootDir%%G4SAIDDATA1.1
set G4NEUTRONHPDATA=%%G4dataset_RootDir%%G4NDL4.5
set G4NEUTRONXSDATA=%%G4dataset_RootDir%%G4NEUTRONXS1.4
set G4PIIDATA=%%G4dataset_RootDir%%G4PII1.3
set G4RADIOACTIVEDATA=%%G4dataset_RootDir%%RadioactiveDecay5.2
set G4REALSURFACEDATA=%%G4dataset_RootDir%%RealSurface2.1
set G4ABLADATA=%%G4dataset_RootDir%%G4ABLA3.1

setx Geant4_DIR %G4_bat_file_dir%install\lib\Geant4-10.4.3\
set Geant4_DIR=%G4_bat_file_dir%install\lib\Geant4-10.4.3\

echo " "
echo "Done"

REM TEST: Compiling and executing example B1

echo "TEST: Compiling and executing example B1"

if not exist %G4_bat_file_dir%\ExampleB1\build mkdir %G4_bat_file_dir%\ExampleB1\build

cd %G4_bat_file_dir%\ExampleB1\build

if exist CMakeCache.txt del CMakeCache.txt

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe ^
-DCMAKE_CONFIGURATION_TYPES=%build_type% ^
-DCMAKE_PREFIX_PATH=%G4_bat_file_dir%\install\lib\Geant4-10.4.3 ^
%G4_bat_file_dir%\ExampleB1\source

%G4_bat_file_dir%\cmake-3.14\bin\cmake.exe --build . --config %build_type%

start "" %G4_bat_file_dir%\ExampleB1\build\%build_type%\exampleB1.exe

PAUSE
